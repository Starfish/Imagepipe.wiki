Welcome to the Imagepipe wiki!

Imagepipe reduces image size and removes exif-tags when you share images. The use of imagepipe is simple:

Whenever you want to share an image, chose Imagepipe from the menu that pops up on your device. Imagepipe loads the image, reduces the size/quality (based on your settings), removes exif-tags and directly prompts you to re-share the modified image.

Therefore, improved privacy and saving on your data plan is just one click away.

The modified images may also be saved locally on your device. 

# Manual
## Privacy
For the privacy statement, [see here](https://codeberg.org/Starfish/Imagepipe/wiki/Privacy-Policy).
## Quickstart
1. Open your gallery app
2. Select an image, select share
3. Chose Imagepipe from the list of applications

The imagepipe application will open and will ask you:
"Allow **Imagepipe** to access photos and media on your device?"

Press "**Allow**".

The image will load, and a **Share** chooser menu will appear.

Now, select the application you want to share the image to (e.g. your email client, your messenger app, or others).

*Congratulations! You have shared your first image using Imagepipe.*
## Workmodes
Imagepipe has three ways you can call it:
### Share an image to Imagepipe
This is the traditional way Imagepipe works: you share the image to Imagepipe, the metadata is stripped off, the image is shrunk and shared onward to the target app.
### Pick an image using Imagepipe
When you are inside a messaging app or an email client, you can also select Imagepipe to pick the image: Imagepipe is launched, and you select the images from the gallery. The images are loaded by Imagepipe, stripped off the metadata, shrunk and returned to the app you were using. 

Should you have disabled *"pipe directly"* (see later on) and load *one* image, you have the opportunity to edit the image first. The *share* icon is replaced by a *checkmark*. Hit the *checkmark* once you are ready, and Imagepipe will return the image to the app you were using.
### From the app launcher
You can also start Imagepipe from the app launcher like every other app. Then, you need to load an image (1) and share it manually (2) by hitting the share button. <br>
<center><img src="https://kaffeemitkoffein.de/resources/workflow01.png" width="240"/></center>

## Troubleshooting
### The taget app does not load the image
Sometimes, the target app also needs the permission to access photos and media. You may have denied it, or perhaps the target app does not properly ask for it. You can go to the app settings of the target app and grant this permission and try again.
### When bulk-sharing, not all images arrive in the target app
Some apps have limits how many images they accept in one share (k9-mail e.g. 50). Try to share a smaller number of images.
## Settings
### Pipe directly: The image is processed automatically and the modified version is directly sent onward.
You can define the default behavior: should Imagepipe just open the image that was shared with it, so that you can edit it before sending onward, or do you want to immediately share the image with the target application? The latter is the default and is enabled, when this option is set.

If you usually edit images manually before sharing, you should disable this option.

Please note that this option only has effect when you share or select a single image. It is *ignored* if you select multiple images to share or pick multiple images using Imagepipe from an other application.
### Auto-Scale: Automatically scale down the image. 
Disabling this may slow down performance and may lead to high memory use. It is highly recommended to keep this on. When loading an image, Imagepipe will immediately scale down the resolution to what you have set. This guarantees good performance, because the original image is not loaded into memory at all, but only a smaller copy.
### Autorotate: Auto-rotate the image.
When enabled, Imagepipe tries to figure out the original image orientation from the exif data or the data it can get from the gallery/mediastore and rotates the image if necessary. Please note that this is not guaranteed, since the image may lack information about orientation. In the latter case, Imagepipe will do nothing with the image but keep the orientation how it is.

When disabled, no auto-rotation will be performed. Please note that the image usually loses information about rotation. As a result, the target app cannot handle any auto-rotation any more. If you are used to auto-rotation features, keep this enabled.
### Apply filter: Auto-apply the last selected filter.
This is disabled by default, and usually only reserved to special use-cases where all images should be altered by a filter. When enabled, all images loaded will get the filter applied immediately. You can change the filter applied in the main app.
### Remove transparency
Replaces transparent pixels with a solid color. This only works for file formats that support a transparent background, like WebP and png. This option does not work with jpeg files. To turn it off, chose the transparent color from the color picker.

### Output format
Defines the file format for image output. This is the processed image that is shared onward by Imagepipe. Imagepipe supports:
- jpeg
- png
- WebP

If your device runs Android(tm) 11 (api 30) or higher, Imagepipe also supports the WebP *lossless* format. Simply make sure the image quality is set to 100, and Imagepipe will apply the WebP lossless format with a maximum compression rate.
### Preview quality and size
When enabled (default), changing the quality in the main app results in a preview that applies this quality immediately to the image. The calculation of the preview may consume quite a lot of resources. If you experience that the app freezes or gets really slow when you change the quality, you can disable this feature here.

### Maximum quality
Here, you can set the maximum value of the bar in the main app that defines the quality. When working with jpeg files, it is recommended to set this to 80 or lower. The reason is that jpeg files with a limited quality may even get larger when you apply a too high quality value to them.
Should you want to produce images in the WebP lossless format, you have to set this to 100 and also the quality to 100 in the main app view.
### Bilinear filtering
This option (enabled by default) applies bilinear filtering when you scale the image within the app or apply a filter to it. This comes with a small performance loss that usually justifies the use of this filtering. It is recommended to keep this enabled unless you experience serious performance issues when resizing images manually.
### Always scale down to screen size
This option guarantees that no image gets loaded with a resolution higher than your device display. Imagepipe can handle larger images, but in many cases your device can't. Smartphones, especially when they are not that expensive, come with a ram size that correlates with the screen resolution. The higher the resolution, the more ram is needed for a device to work properly. 
When enabled, this option overrides your maximum image size if the latter is larger than your display.
It is highly recommended to keep this enabled for daily use. You may need to disable it occasionally; a typical use-case is to cut out a frame of an image using the original image resolution.
Should your device not be able to handle the image size, the preview of the image in the app will fail silently and you will only see a gray background.
### File saving
Imagepipe basically can name the target image in three ways:
1. take a generic name (e.g. "Imagepipe") and append or prefix it with a number and/or date: results e.g. in *Imagepipe_1.jpeg*
2. keep the original file name and append or prefix it with a number and/or date: results e.g. in *IMG20230312_131107_1.jpeg*
3. generate a random name (always 16 chars): results e.g. in *d4er6zhl77zhggf44.jpeg*

### Generic filename
If you have chosen #1 (see above), this option determines what the "generic name" will be. 
### Numbering
Here, you chose how the output files should be named. 
### Auto-save
When enabled, prior to sharing, images are saved and made available in the gallery. Basically, this is the mode Imagepipe was working before the introduction of the storage access framework, also called "scoped storage". When enabled, each image processed will also be saved in the "Pictures/Imagepipe" folder on your device. Please note, that all other apps that have access to media files on your device will be able to access the saved pictures. Please also note that uninstalling Imagepipe *will not* remove those images.
This is not of much concern, because the saved images are ripped off the metadata. However, should you worry about the contents of the images, it is advised to disable this option (default). Should you want to recycle the images and/or keep the copies, it may be feasible to enable this.
### Clear after sharing
Clears the internal app cache holding the latest image(s) after a target app for sharing was selected by the user. This will *not* remove any images saved to the gallery when auto-save (see above) is enabled. This option may not be available on older devices.
### Allowed Exif Tags
You may specify certain exif tags that will be kept in all saved and shared images. You need to enter the original exif tag constant values from [here](https://developer.android.com/reference/android/media/ExifInterface), separated by the '#' character, without any trailing spaces. Please use with caution, Imagepipe can only keep tags it knows (see source code if you are in doubt), tags unknown to Imagepipe will be ignored.

## User interface
This chapter will explain the main menu and the image editor. The main menu options are displayed in the upper app bar (known as the action bar) and expanded by pressing the three dot icon. The image editor is visible below the app bar.

### Main Menu

#### Load a picture from gallery
Loads one picture from your gallery app. Should you have more than one gallery app installed, you will be prompted which one to use. 
#### Share
Shares the picture. A chooser will pop up, where you can select the recipient or the target app. 
#### Save
Saves a copy of the picture to your media store. This copy will be available to all apps that can access your media.
#### Settings
Opens the settings. See above for details about the settings.
#### Image tags
Shows known EXIF image tags. Please note that some tags not known to Imagepipe may not be shown. Alt tags will be removed when sharing, including the unknown ones.
#### About...
Opens a dialog with information about Imagepipe.
#### Licences
Displays the open source licences of the components used.
#### Clear data
Clears the picture cache including the visible picture. This cannot be undone.
### Image editor

Although in the 1st place Imagepipe is designed to quickly rip off metadata and shrink an image before sharing, it is also a kind of [Swiss Army Knife](https://en.wikipedia.org/wiki/Swiss_Army_knife) for simple image manipulation. The goal is to keep the application small, but still allow for some basic image manipulation functions.

Most buttons trigger two actions: one by short-pressing it, the other one by long-pressing the button.<br>
<center><img src="https://kaffeemitkoffein.de/resources/image-editor.png" width="240"/></center><br>

Should a small tag-symbol (#) be visible: it indicates that some exif-tags were manually selected to be kept when sharing and/or saving the picture.

#### 1. Crop

**short-press:** enable cropping. Move the borders like desired. Hold down in the center to move the box. Press the button again to perform cropping.

<center><img src="https://kaffeemitkoffein.de/resources/crop.png" width="240"/></center><br>

**long-press:** opens a menu that allows you to enter a fixed aspect ratio for cropping. Enter e.g. "4:3" or "16:9". As a result, the cut frame will keep this ratio. Open this menu again and delete the text to disable the fixed aspect ratio.<br>

<center><img src="https://kaffeemitkoffein.de/resources/aspect_ratio.png" width="240"/></center><br>

#### 2. Blur

**short-press:** enable blurring. Imagepipe uses the native [BlurMaskFilter](https://developer.android.com/reference/android/graphics/BlurMaskFilter) that applies a [gaussian blur](https://github.com/google/skia/blob/main/include/effects/SkBlurMaskFilter.h) to the area. 

**long-press:** allows for selection of the brush size. Note that for paint & blur the same brush size is used. Changing one also changes the other.

<center><img src="https://kaffeemitkoffein.de/resources/blur_size.png" width="240"/></center><br>

#### 3. Write Text

**short-press:** enable putting text on the canvas. This works like a stamp: click/touch the left-bottom edge where you expect the text to appear. 

The text size is derived from the brush size. Change the brush size for larger or smaller text size. 

When the text won't fit and would be cut off by the right image border, the text size is adjusted to make the text fit.

**long-press:** opens a menu. Enter the text here and chose any desired text effects. The text size is derived from the brush size.

<center><img src="https://kaffeemitkoffein.de/resources/text.png" width="240"/></center><br>

#### 4. Paint

**short-press:** enable painting. Simply move your finger over the canvas to paint.

**long-press:** opens a menu. You can adjust the brush size here. Please note that the same brush size is used for blurring. Changing one of them always changes both.

#### 5. Color selector

The icon shows the current color.

**short-press** to open a simple color tool that allows you to get one of 24 preset colors:

<br><center><img src="https://kaffeemitkoffein.de/resources/colors00.png" width="240"/></center><br>

Simply tap on the desired color to select it.

**long-press** to open a more complex color selector:

<br><center><img src="https://kaffeemitkoffein.de/resources/colors01.png" width="240"/></center><br>

1. Select the color by clicking/tapping on the main palette.
2. Adjust the color brightness.
3. Alternatively, pick one of 16 default colors to start with.
4. The two color slots at the bottom show a history of the last two colors used.
5. This is the previous (current) color. Tap it to set your selection back to this color.
6. This is the target color. Tap it to select it and close the window.

#### 6. Color picker

Select this tool to pick a color from the image. Once selected, simply tap/click on the desired image area to fetch the color.

#### 7. Undo

Undo your painting actions done on the canvas.

#### 8. Redo

Redo any undone actions.

#### 10. Select quality

By sliding this bar, you can set the target image quality. The *maximum* value of this slider is determined by the setting "maximum quality". The idea is that loading an jpeg file with a limited quality and setting the target quality to very high values like 90 or 100 can *increase* the target image size instead of shrinking it. By setting the *maximum quality* to something else than 100 makes it easier to handle jpeg files.

Should your device have at least the api level 30, choosing a quality of 100 and using the WebP format will result in a *lossless* WebP file format with maximum compression. Note that size will be usually modified anyway, so the output image is not a *copy* in terms of quality.

When using png files, this feature is *disabled*, because png is a lossless format.

The chosen quality is applied to the preview (default); should you experience a slow user interface, you can disable this quality preview in the settings.

#### 11. Set image size

The diameters of the output image are defined by the method and a size value. The method is displayed in the middle between the quality bar and the number field. By tapping on the text in the middle, you can chose one of five methods.

You have five choices how to set the output image size:
1. width: the image width will be set to the value in the input field right to it (or a lesser value, if the loaded image is smaller). The height will be calculated to keep the aspect ratio.
2. height: the image height will be set to the value in the input field right to it (or a lesser value, if the loaded image is smaller). The width will be calculated to keep the aspect ratio.
3. max. width & height: the image will be resized so that neither the height nor the width will be larger than the value. The aspect ratio is preserved. This is the default setting.
4. min. width & height: the image will be resiszed so that the *smaller* of width and height will have this value. The aspect ratio is preserved.
5. %: the image will be resized to this percentage (100% is the original size). 

When "*Always scale down to screen size*" is enabled (default), the larger diameter of the screen size overrides the value in the input field. This means that if the resulting image is still too large, it will be resized accordingly.

#### 12. Apply filter

**short-press**: apply the selected filter.

**long-press**: opens a menu that allows to select one of 14 filters. Tap/click on the filter to select it. Select none to disable a filter at all. 

Note: the color reduction filters "2 colors", "16 colors", "64 colors", "64 colors", "256 colors" determine optimal target palettes based on the colors found in the image, while the "16 fixed colors" filter uses the default Imagepipe palette as target. All the mentioned filters apply dithering (error diffusion) for better results. 

<br><center><img src="https://kaffeemitkoffein.de/resources/filters.png" width="240"/></center><br>

Should you desire to apply a filter to a bulk of images when sharing multiple files, you can enable "*apply filter: Auto-apply the last selected filter*" in the settings.

#### 13. Flip image

A **short-press** flips the image horizontally, a **long-press** flips it vertically.

#### 14. Rotate anti-clockwise

**short-press**: Rotates the image anti-clockwise.

**long-press**: Opens a dialog that allows for rotation by any angle:
<br><center><img src="https://kaffeemitkoffein.de/resources/crop_ui.png" width="240"/></center><br>

1. Move the slider to the left to rotate the image clock-wise, move it to the right to rotate anti-clock-wise.
2. Tick "crop corners" to automatically get the part of the image in the center without visible black corners.

#### 15. Rotate clockwise

**short-press**: Rotates the image clockwise.

**long-press**: Opens a dialog that allows for rotation by any angle. See above.

#### 16. Shrink

Apply the target image size (see number 11) to the image.

#### 17. Reload from original

**short-press**: Reloads the image from the original, if possible, and processes it like desired. 

**long-press**: Reloads the image from the original at a larger size. This respects the "*Always scale down to screen size*" setting, but does not further scale down the image upon loading. Other effects like auto-rotation, applying of filters and others  will be applied according to the settings. This option is extremely useful to load an image before cropping out a small part of it to achieve better results.

Please note that this action may fail, since Imagepipe might not be able to access the original media again. 

# Footnote 

Some of the icons used in the screenshots are Copyright (c) Google Inc. and licensed under the Apache 2.0 license. Android is a trademark of Google LLC.