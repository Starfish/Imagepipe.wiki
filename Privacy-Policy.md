# Privacy policy
## Data stored locally

The Imagepipe program stores the following data **locally on the device**:
- the last images opened with Imagepipe and, when drawing, the different versions of the image,
- the settings.

If "Save automatically" is activated and/or an image is saved, the image files created by Imagepipe are saved in the MediaStore (the "gallery" on the device). There, other applications may be able to access these files, if they have the permission to do so.

When the user actively shares an image, the image file is made available to the selected application.

Beyond that, Imagepipe does not collect or send any data. 

## Delete data

The last opened and/or edited images can be deleted in the app with the menu item "Delete data".

The settings can be deleted from the app information ("App Info") of the device by the menu item "Clear Memory Contents" or by uninstalling Imagepipe.

The images that may be stored in the MediaStore (the "Gallery" on the device) can be deleted from the Gallery. 

---

# Datenschutzerklärung

## Lokal gespeicherte Daten

Das Programm Imagepipe speichert folgende Daten **lokal auf dem Gerät**:
- die zuletzt mit Imagepipe geöffneten Bilder und beim Zeichnen die verschiedenen Versionen des Bildes,
- die Einstellungen.

Wenn "Automatisch speichern" aktiviert ist und/oder ein Bild gespeichert wird, werden die von Imagepipe erstellten Bilddateien im MediaStore (der "Gallerie" auf dem Gerät) gespeichert. Dort können ggf. andere Anwendungen auf diese Dateien zugreifen, sofern sie dazu die Berechtigung haben.

Wenn der Benutzer ein Bild aktiv teilt, wird die Bilddatei der ausgewählten Anwendung zur Verfügung gestellt.

Darüber hinaus sammelt und versendet Imagepipe keine Daten. 

## Daten löschen

Die zuletzt geöffneten und/oder bearbeiteten Bilder können in der App mit dem Menüpunkt "Daten löschen" gelöscht werden.

Die Einstellungen können aus der App-Information ("App-Info") des Gerätes heraus gelöscht werden durch den Menüpunkt "Speicherinhalt löschen" oder durch Deinstallieren von Imagepipe.

Die im MediaStore (der "Gallerie" auf dem Gerät) eventuell gespeicherten Bilder können in der Gallerie gelöscht werden. 

